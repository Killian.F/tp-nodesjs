'use strict';

const Joi = require('joi');
const user = Joi.object().keys({
    login : Joi.string().required(),
    password : Joi.string().min(8).alphanum().required(),
    email : Joi.string().email().required(),
    firstname : Joi.string().required(),
    lastname : Joi.string().required(),
    company : Joi.string(),
    fonction : Joi.string(),
});

module.exports=user;
