
exports.up = function(knex, Promise) {
    return knex
        .schema
        .createTable( 'users', function( usersTable ) {

            // Primary Key
            usersTable.increments();

            // Data
            usersTable.string( 'login', 50 ).notNullable().unique();
            usersTable.string( 'password', 50 ).notNullable();
            usersTable.string( 'email', 50 ).notNullable().unique();
            usersTable.string( 'firstname', 50 ).notNullable();
            usersTable.string( 'lastname', 50 ).notNullable();
            usersTable.string( 'company', 50 ).notNullable();
            usersTable.string( 'function', 50 ).notNullable();

        } )
};

exports.down = function(knex, Promise) {
    return knex
        .schema
        .dropTableIfExists('users');
};


