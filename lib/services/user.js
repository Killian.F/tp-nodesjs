'use strict';
const faker = require('faker');
// const { fonctionCriptage } =  require('hapi-encrypt/encrypt');
const { Service } = require('schmervice');
module.exports = class UserService extends Service {
    async initialize(){ // CALLED ON SERVER INITIALIZATION (onPreStart)
// set up stuff here
    }
    async teardown(){ // CALLED ON SERVER STOP (OnPostStop)
// tear down stuff here
    }
    hello(user){
        return `Hello ${user.firstName}`;
    }

    async add(objet) {
        const { User } = this.server.models();
        return await User.query().insert(objet);
        // return await knex('users').insert(objet);
    }

    async getAll() {
        const { User }= this.server.models();
        return await User.query();
    }

    async getUserById(id){
        const { User } = this.server.models();
        return await User.query().where('id', '=', id);
    }

    async delete(user, id) {
        const { User } = this.server.models();
        return await User.query().delete().where('id', '=', id);
    }

    async update(user, id) {
        const { User } = this.server.models();
        return await User.query().patch(user).where('id', '=', id);
    }

    async generate(){
        const tabUser=[];
        for(let i=0; i<100; i++){
            const user = {
                login : faker.internet.userName(),
                password : faker.internet.password(),
                email : faker.internet.email(),
                firstname : faker.name.firstName(),
                lastname : faker.name.lastName(),
                company : faker.company.companyName(),
                function : faker.company.catchPhrase()
            };
            tabUser.push(user);
        }
        return 'tabUser';
    }
};
