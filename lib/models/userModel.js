'use strict';
const { Model } = require('objection');
const userSchema = require('../schemas/users');
module.exports = class User extends Model {
    static get tableName() {
        return 'users';
    }
    static get joiSchema() {
        return userSchema;
    }
}
