//http://localhost:3000/documentation

const userSchema = require('../schemas/users');
const Joi = require('joi');

module.exports = [
    {
        method : 'get',
        path : '/users',
        options:{
            tags: ['api']
        },
        async handler(request, h) {
            const { userService } = request.services();
            return await userService.getAll();
        }
    },
    {
        method: 'post',
        path: '/users-add',
        options: {
            tags: ['api'],
            validate: {
                payload: userSchema
            }
        },
        async handler(request, h){
            const { userService } = request.services();
            return await userService.add(request.payload);
        }
    },
    {
        method: 'get',
        path: '/users/generate',
        options: {
            tags: ['api'],
        },
        async handler (request, h) {
            const { userService } = request.services();
            return await userService.generate();
        }
    },
    {
        method: 'delete',
        path: '/delete/{id}',
        options:{
            tags:['api'],
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        async handler(request, h){
            const { userService } = request.services();
            await userService.delete(request.params.id);
            return 'Deleted successfully';
        }
    },
    {
        method: 'put',
        path: '/users-edit/{id}',
        options:{
            tags:['api'],
            validate: {
                payload: userSchema,
                params: {
                    id: Joi.number().required()
                }
            }
        },
        async handler(request, h){
            const { userService } = request.services();
            await  userService.update(request.params.id, request.payload);
            return 'Updated successfully';
        }

    },
    {
        method: 'get',
        path: '/users/{id}',
        options: {
            tags: ['api'],
            validate : {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        async handler(request, h){
            const { userService } = request.services();
            return await userService.getUserById(request.params.id);
        }
    }
]
